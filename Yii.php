<?php
/**
 * Yii bootstrap file.
 *
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

@ini_set('default_charset', 'UTF-8');

require(__DIR__ . '/vendor/autoload.php');

use Symfony\Component\Dotenv\Dotenv;
use Symfony\Component\Dotenv\Exception\PathException;

$dotEnv = new Dotenv(true);

try {
    $dotEnv->load(__DIR__ . '/.env');
} catch (PathException $exception) {
    $dotEnv->load(__DIR__ . '/.env.dist');
}

defined('YII_ENV') or define('YII_ENV', getenv('YII_ENV') ?: 'dev');
defined('YII_ENV_STAGE') or define('YII_ENV_STAGE', YII_ENV === 'stage');
defined('YII_DEBUG') or define('YII_DEBUG', YII_ENV === 'dev' || PHP_SAPI === 'cli');

/**
 * Yii is a helper class serving common framework functionalities.
 *
 * It extends from [[\yii\BaseYii]] which provides the actual implementation.
 * By writing your own Yii class, you can customize some functionalities of [[\yii\BaseYii]].
 *
 * @method app\modules\base\components\Logger getLogger() static
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class Yii extends \yii\BaseYii
{
    /**
     * @var yii\console\Application|\yii\web\Application
     */
    public static $app;

    /**
     * @inheritdoc
     */
    public static function debug($message, $category = 'application')
    {
        $logger = static::getLogger();
        $logger->log($message, $logger::LEVEL_TRACE, $category);
    }
}

spl_autoload_register(['Yii', 'autoload'], true, true);

Yii::$classMap = require(YII2_PATH . '/classes.php');
Yii::$container = new yii\di\Container();

Yii::setAlias('@app', __DIR__);
