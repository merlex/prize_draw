<?php

use yii\web\Application;

require(__DIR__ . '/../Yii.php');

$config = require(__DIR__ . '/../config/web.php');
$application = new Application($config);
$application->run();
