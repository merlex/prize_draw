<?php

require(__DIR__ . '/Yii.php');

class Installer extends \yii\composer\Installer
{
    /**
     * @inheritdoc
     */
    public static function postInstall($event)
    {
        $params = $event->getComposer()->getPackage()->getExtra();
        if (isset($params[__METHOD__]) && is_array($params[__METHOD__])) {
            foreach ($params[__METHOD__] as $method => $args) {
                call_user_func_array([__CLASS__, $method], (array)$args);
            }
        }
    }

    /**
     * Make directories
     */
    public static function makeDirs()
    {
        foreach (func_get_args() as $path) {
            echo "mkdir('$path')...";
            if (!is_dir($path)) {
                try {
                    echo mkdir($path, 0777, true)
                        ? "done.\n" : "fail.\n";
                } catch (Exception $e) {
                    echo $e->getMessage() . "\n";
                }
            } else {
                echo "exists.\n";
            }
        }
    }

    /**
     * Merge configuration files
     * @param array $files
     * @param string $env
     */
    public static function mergeFiles(array $files, $env = YII_ENV)
    {
        foreach ($files as $destination => $sources) {
            echo "Building file $destination ";
            $array = [];
            foreach ($sources as $source) {
                $source = strtr($source, ['{env}' => $env]);
                if (!is_file($source)) {
                    continue;
                }
                echo "+ $source ";
                $array = \yii\helpers\ArrayHelper::merge($array, include $source);
            }
            try {
                $content = "<?php\n/* Generated @ " . date('r') . " */\n"
                    . 'return \yii\helpers\ArrayHelper::merge('
                    . \yii\helpers\VarDumper::export($array)
                    . ', include(__DIR__ . \'/local.php\'));';
                echo file_put_contents($destination, $content) ? "- done.\n" : "- fail.\n";
            } catch (Exception $e) {
                echo $e->getMessage() . "\n";
            }
        }
    }

    /**
     * @param array $files
     */
    public static function moveFiles(array $files)
    {
        foreach ($files as $from => $to) {
            echo "Moving file $from to $to ";
            $fromFile = Yii::getAlias($from);
            if (!is_file($fromFile)) {
                echo "- skip.\n";
                continue;
            }
            $toFile = Yii::getAlias($to);
            if (is_file($toFile)) {
                unlink($toFile);
            }
            try {
                echo rename($fromFile, $toFile)
                    ? "- done.\n" : "- fail.\n";
            } catch (Exception $e) {
                echo $e->getMessage() . "\n";
            }
        }
    }
}
