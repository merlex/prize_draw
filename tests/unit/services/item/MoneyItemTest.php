<?php

use app\modules\base\models\fixtures\OrderFixture;
use app\modules\base\models\fixtures\PrizeFixture;
use app\modules\base\services\item\MoneyItem;
use Codeception\Test\Unit;

class MoneyItemTest extends Unit
{
    /**
     * @var UnitTester
     */
    protected $tester;

    /**
     * @return array
     */
    public function _fixtures()
    {
        return [
            'orders' => OrderFixture::class,
            'prizes' => PrizeFixture::class,
        ];
    }

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testConvert()
    {
        $order = $this->tester->grabFixture('orders', 'order1');
        $model = new MoneyItem($order);
        Yii::$app->settings->set('SettingsForm', 'coefficient', 2);
        $model->convert();
        $this->assertEquals(20, $order->value);
    }
}
