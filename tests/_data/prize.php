<?php
return [
    'prize1' => [
        "id" => 5,
        "title" => "Денежный приз",
        "description" => "Денежный приз",
        "type" => "money",
        "available" => 125,
        "status" => "active",
        "createdAt" => "2020-12-17 23:38:00.0",
        "updatedAt" => "2020-12-19 22:18:34.0"
    ],
    'prize2' => [
        "id" => 4,
		"title" => "Бонусные баллы",
		"description" => "Бонусные баллы",
		"type" => "points",
		"available" => 1000,
		"status" => "active",
		"createdAt" => "2020-12-17 23:38:00.0",
		"updatedAt" => "2020-12-17 23:38:00.0"
    ],
];
