<?php

namespace app\modules\admin\controllers;

use app\modules\admin\models\forms\SettingsForm;
use app\modules\base\components\WebController;
use yii2mod\settings\actions\SettingsAction;

/**
 * Default controller for the `admin` module
 */
class DefaultController extends WebController
{
    /**
     * @inheritdoc
     */
    protected function rules()
    {
        return [
            [
                'allow' => true,
                'actions' => ['index', 'settings'],
                'roles' => ['admin'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'settings' => [
                'class' => SettingsAction::class,
                'modelClass' => SettingsForm::class,
            ],
        ];
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        switch (true) {
            case \Yii::$app->user->isGuest:
                $result = $this->redirect(['/admin/default/login']);
                break;
            case \Yii::$app->getUser()->can('admin-only'):
                $result = $this->redirect(['/admin/prize/index']);
                break;
            case \Yii::$app->getUser()->can('user-only'):
            default:
                $result = $this->redirect(['/front/default/index']);
                break;
        }

        return $result;
    }
}
