<?php

namespace app\modules\admin\models\forms;

use Yii;
use yii\base\Model;

class SettingsForm extends Model
{
    /**
     * @var numeric коэффициент конвертации в баллы лояльности
     */
    public $coefficient;

    /**
     * @var numeric максимальная сумма денежного приза
     */
    public $maxMoney;

    /**
     * @var numeric максимальная сумма баллов
     */
    public $maxPoints;

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['coefficient', 'maxMoney', 'maxPoints'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(): array
    {
        return [
            'coefficient' => 'Коэффициент',
            'maxMoney' => 'Максимальная сумма денежного приза',
            'maxPoints' => 'Максимальная сумма баллов'
        ];
    }
}
