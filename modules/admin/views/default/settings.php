<?php

use app\modules\admin\models\forms\SettingsForm;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $model SettingsForm */
/* @var $this View */

$this->title = 'Настройки';
?>

<div class="settings-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <?= $form->field($model, 'coefficient', ['options' => ['class' => 'col-md-4']])
            ->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'maxMoney', ['options' => ['class' => 'col-md-4']])
            ->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'maxPoints', ['options' => ['class' => 'col-md-4']])
            ->textInput(['maxlength' => true]) ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
