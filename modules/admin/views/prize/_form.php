<?php

use app\modules\base\models\Prize;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\base\models\Prize */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="prize-form box box-primary">
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body table-responsive">

        <div class="row">
            <?= $form->field($model, 'title', ['options' => ['class' => 'col-md-4']])
                ->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'available', ['options' => ['class' => 'col-md-4']])
                ->textInput() ?>
        </div>
        <div class="row">
            <?= $form->field($model, 'description', ['options' => ['class' => 'col-md-6']])
                ->textarea(['rows' => 6]) ?>
        </div>
        <div class="row">
            <?= $form->field($model, 'type', ['options' => ['class' => 'col-md-4']])
                ->dropDownList(Prize::typeNames()) ?>

            <?= $form->field($model, 'status', ['options' => ['class' => 'col-md-4']])
                ->dropDownList(Prize::statusNames()) ?>
        </div>

    </div>
    <div class="box-footer">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
