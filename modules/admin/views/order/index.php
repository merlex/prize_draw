<?php

use app\modules\base\models\Identity;
use app\modules\base\models\Order;
use app\modules\base\models\Prize;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Orders';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-index box box-primary">
    <?php Pjax::begin(); ?>
    <div class="box-header with-border">
        <?= Html::a('Create Order', ['create'], ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",
            'columns' => [
                'id',
                [
                    'attribute' => 'userId',
                    'value' => 'user.fullname',
                    'filter' => ArrayHelper::map(Identity::find()->role(Identity::ROLE_USER)->active()->all(), 'id', 'fullname'),
                ],
                [
                    'attribute' => 'prizeId',
                    'value' => 'prize.title',
                    'filter' => ArrayHelper::map(Prize::find()->active()->all(), 'id', 'title'),
                ],
                'value',
                [
                    'attribute' => 'status',
                    'value' => 'statusName',
                    'filter' => Order::statusNames(),
                ],
                // 'createdAt',
                // 'updatedAt',

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    </div>
    <?php Pjax::end(); ?>
</div>
