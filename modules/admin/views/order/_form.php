<?php

use app\modules\base\models\Identity;
use app\modules\base\models\Order;
use app\modules\base\models\Prize;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\base\models\Order */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="order-form box box-primary">
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body table-responsive">

        <div class="row">
            <?php //TODO Необходимо передеалть селект на поле с динамической подгрузкой, например Select2 ?>
            <?= $form->field($model, 'userId', ['options' => ['class' => 'col-md-4']])
                ->dropDownList(ArrayHelper::map(Identity::find()->role(Identity::ROLE_USER)->active()->all(), 'id', 'fullname')) ?>

            <?php //TODO Необходимо передеалть селект на поле с динамической подгрузкой, например Select2 ?>
            <?= $form->field($model, 'prizeId', ['options' => ['class' => 'col-md-4']])
                ->dropDownList(ArrayHelper::map(Prize::find()->active()->all(), 'id', 'title')) ?>
        </div>
        <div class="row">
            <?= $form->field($model, 'value', ['options' => ['class' => 'col-md-4']])
                ->textInput() ?>

            <?= $form->field($model, 'status', ['options' => ['class' => 'col-md-4']])
                ->dropDownList(Order::statusNames(), ['prompt' => '']) ?>
        </div>
    </div>
    <div class="box-footer">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
