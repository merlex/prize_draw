<?php


/* @var $this yii\web\View */
/* @var $model app\modules\base\models\Identity */

$this->title = 'Добавить пользователя';
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="identity-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
