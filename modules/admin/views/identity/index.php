<?php

use app\modules\base\models\Identity;
use kartik\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\IdentitySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Пользователи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="identity-index">

    <p><?= Html::a('Добавить пользователя', ['create'], ['class' => 'btn btn-success']) ?></p>

    <?= GridView::widget([
        'options' => ['id' => 'identity-grid'],
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'username',
            [
                'attribute' => 'role',
                'value' => 'roleName',
                'filter' => Identity::getRoles(),
            ],
            'firstname',
            'lastname',
            'isActive:boolean',
            //'createdAt',
            //'updatedAt',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {delete}',
            ],
        ],
    ]); ?>
</div>
