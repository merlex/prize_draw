<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\base\models\Identity;

/* @var $this yii\web\View */
/* @var $model Identity */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="identity-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <?= $form->field($model, 'username', ['options' => ['class' => 'col-md-4']])
            ->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'newPassword', ['options' => ['class' => 'col-md-4']])
            ->passwordInput(['value' => '']) ?>
        <?= $form->field($model, 'role', ['options' => ['class' => 'col-md-4']])
            ->dropDownList(Identity::getRoles()) ?>
    </div>
    <div class="row">
        <?= $form->field($model, 'firstname', ['options' => ['class' => 'col-md-4']])
            ->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'lastname', ['options' => ['class' => 'col-md-4']])
            ->textInput(['maxlength' => true]) ?>
    </div>

    <div class="row">
        <?= $form->field($model, 'isActive', ['options' => ['class' => 'col-md-4']])
            ->checkbox() ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
