<?php $this->beginContent('@app/modules/admin/views/layouts/main.php'); ?>

<div class="row">
    <div class="col-lg-9">
        <?= $content; ?>
    </div>
    <div class="col-lg-3">
        <?php if (isset($this->blocks['sidebar'])) : ?>
            <?= $this->blocks['sidebar']; ?>
        <?php endif; ?>
    </div>
</div>

<?php $this->endContent(); ?>
