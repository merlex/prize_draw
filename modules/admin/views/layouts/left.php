<aside class="main-sidebar">

    <section class="sidebar">

        <?= dmstr\widgets\Menu::widget(
            [
                'encodeLabels' => false,
                'options' => ['class' => 'sidebar-menu tree', 'data-widget' => 'tree', 'data-follow-link' => "true"],
                'items' => [
                    ['label' => 'Меню', 'options' => ['class' => 'header']],
                    ['label' => 'Призы', 'icon' => 'gift', 'url' => ['/admin/prize/index']],
                    ['label' => 'Заказы', 'icon' => 'donate', 'url' => ['/admin/order/index']],
                    ['label' => 'Пользователи', 'icon' => 'users', 'url' => ['/admin/identity/index']],
                    ['label' => 'Настройки', 'icon' => 'cogs', 'url' => ['/admin/default/settings']],

                    ['label' => 'Выход', 'icon' => 'sign-out-alt', 'url' => ['/front/default/logout'], 'visible' => !Yii::$app->user->isGuest],
                ],
            ]
        ) ?>

    </section>

</aside>
