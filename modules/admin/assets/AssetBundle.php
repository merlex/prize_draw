<?php

namespace app\modules\admin\assets;

use dmstr\web\AdminLteAsset;
use yii\bootstrap\BootstrapAsset;
use yii\bootstrap\BootstrapPluginAsset;
use yii\web\YiiAsset;

class AssetBundle extends \yii\web\AssetBundle
{

    public $sourcePath = '@app/modules/admin/web';

    public $css = [
        'css/admin.css'
    ];

    public $js = [
        'js/admin.js',
    ];

    public $depends = [
        FontAwesomeAsset::class,
        YiiAsset::class,
        BootstrapAsset::class,
        BootstrapPluginAsset::class,
        AdminLteAsset::class,
    ];
}
