<?php

namespace app\modules\admin\assets;

use yii\web\View;

class FontAwesomeAsset extends \yii\web\AssetBundle
{
    public $sourcePath = '@bower/fontawesome';

    public $css = [
        'css/svg-with-js.css',
    ];

    public $js = [
        'js/all.js',
    ];

    public $jsOptions = [
        'position' => View::POS_HEAD,
    ];
}
