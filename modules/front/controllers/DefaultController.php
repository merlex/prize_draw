<?php

namespace app\modules\front\controllers;

use app\modules\base\components\WebController;
use app\modules\base\models\Identity;
use app\modules\base\models\Order;
use app\modules\base\services\OrderService;
use app\modules\base\services\PrizeService;
use yii\base\DynamicModel;
use yii\data\ActiveDataProvider;
use yii\web\Response;

/**
 * Default controller for the `base` module
 */
class DefaultController extends WebController
{
    public $layout = 'main';

    /**
     * @inheritdoc
     */
    protected function rules()
    {
        return [
            [
                'allow' => true,
                'actions' => ['index', 'create', 'reject', 'send', 'convert'],
                'roles' => ['user', 'admin'],
            ],
            [
                'allow' => true,
                'actions' => ['login'],
                'roles' => ['?'],
            ],
            [
                'allow' => true,
                'actions' => ['logout'],
                'roles' => ['@'],
            ],
        ];
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Order::find()->andWhere(['userId' => \Yii::$app->user->id]),
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     *
     */
    public function actionCreate()
    {
        try {
            if ($order = OrderService::instance()->getRandom(\Yii::$app->user->identity)) {
                \Yii::$app->getSession()->addFlash('info', 'Поздравляем! Ваш приз: ' . $order->getTitle());
                return $this->redirect(['index']);
            }
        } catch (\Exception $e) {
            \Yii::$app->getSession()->addFlash('error', $e->getMessage());
        }
        return $this->redirect(['index']);
    }

    /**
     *
     */
    public function actionReject($id)
    {
        $order = Order::find()
            ->andWhere(['status' => Order::STATUS_ACCEPTED])
            ->andWhere(['id' => $id])
            ->one();
        if ($order) {
            if (OrderService::instance()->reject($order)) {
                \Yii::$app->getSession()->addFlash('info', 'Ваш приз ' . $order->prize->title . ' отменен');
                return $this->redirect(['index']);
            }
        }
        \Yii::$app->getSession()->addFlash('error', 'Ошибка! Попробуйте позже.');
        return $this->redirect(['index']);
    }

    /**
     *
     */
    public function actionSend($id)
    {
        $order = Order::find()
            ->andWhere(['status' => Order::STATUS_ACCEPTED])
            ->andWhere(['id' => $id])
            ->one();
        if ($order) {
            if (OrderService::instance()->send($order)) {
                \Yii::$app->getSession()->addFlash('info', 'Ваш приз ' . $order->prize->title . ' отправлен');
                return $this->redirect(['index']);
            }
        }
        \Yii::$app->getSession()->addFlash('error', 'Ошибка! Попробуйте позже.');
        return $this->redirect(['index']);
    }

    /**
     *
     */
    public function actionConvert($id)
    {
        $order = Order::find()
            ->andWhere(['status' => Order::STATUS_ACCEPTED])
            ->andWhere(['id' => $id])
            ->one();
        if ($order) {
            if (OrderService::instance()->convert($order)) {
                \Yii::$app->getSession()->addFlash('info', 'Ваш приз ' . $order->prize->title . ' конвертирован');
                return $this->redirect(['index']);
            }
        }
        \Yii::$app->getSession()->addFlash('error', 'Ошибка! Попробуйте позже.');
        return $this->redirect(['index']);
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionLogin()
    {
        $model = new DynamicModel(['username', 'password', 'rememberMe']);

        $model->addRule(['username', 'password'], 'string')
            ->addRule('username', 'required', ['message' => 'Укажите имя пользователя'])
            ->addRule('password', 'required', ['message' => 'Укажите пароль'])
            ->addRule('rememberMe', 'safe');

        if ($model->load(\Yii::$app->getRequest()->post()) && $model->validate()) {
            $identity = Identity::findIdentityByUsernameAndPassword($model->{'username'}, $model->{'password'});

            if ($identity) {
                $duration = !$model->{'rememberMe'} ? (60 * 60 * 24 * 30) : 0;

                \Yii::$app->getUser()->login($identity, $duration);
                switch (true) {
                    case \Yii::$app->getUser()->can('admin-only'):
                        $redirect = ['/admin/prize/index'];
                        break;
                    case \Yii::$app->getUser()->can('user-only'):
                    default:
                        $redirect = ['/front/default/index'];
                        break;
                }
                return $this->redirect(\Yii::$app->getUser()->getReturnUrl($redirect));
            } else {
                $model->addError('password', 'Неверное имя пользователя или пароль');
            }
        }

        $this->layout = 'main-login';

        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * @return Response
     */
    public function actionLogout()
    {
        \Yii::$app->getUser()->logout();

        return $this->redirect(['login']);
    }
}
