<?php

use app\modules\front\components\MainAsset;
use dmstr\widgets\Alert;
use yii\helpers\Html;
use yii\helpers\Url;

//MainAsset::register($this);
app\modules\admin\assets\AssetBundle::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= Html::encode($this->title ? $this->title : \Yii::$app->name) ?></title>
    <?= Html::csrfMetaTags() ?>

    <?php $this->head() ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<?php $this->beginBody() ?>
<div class="wrapper layout-top-nav">
    <header class="main-header">
        <?= Html::a('<span class="logo-mini">PD</span><span class="logo-lg">' . Yii::$app->name . '</span>', ['/admin'], ['class' => 'logo']) ?>
        <nav class="navbar navbar-static-top" role="navigation">
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">

                    <!-- Messages: style can be found in dropdown.less-->
                    <li class="dropdown notifications-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="glyphicon glyphicon-star"></i>
                            <span class="label label-warning"><?= Yii::$app->user->identity->points; ?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">
                                У вас <?= Yii::$app->user->identity->points; ?> балла(ов)
                            </li>
                        </ul>
                    </li>
                    <li class="user-info">
                        <a href="#">
                            <?= Yii::$app->user->identity->fullname; ?>
                        </a>
                    </li>
                    <li class="logout">
                        <a href="<?= Url::to('logout'); ?>">Выход</a>
                    </li>
                </ul>

            </div>
        </nav>
    </header>

    <div class="content-wrapper">
        <section class="content">
            <?= Alert::widget() ?>
            <?= $content ?>
        </section>
    </div>

    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Версия</b> 1.0
        </div>
        <strong><?= date('Y') ?> <a href="http://prize-draw.loc" target="_blank">Prize Draw</a>.</strong> Все права
        защищены.
    </footer>
</div>
<?php $this->endBody() ?>
<?= Html::endTag('body') ?>
</html>
<?php $this->endPage() ?>
