<?php

use app\modules\base\models\Order;
use app\modules\base\models\Prize;
use kartik\grid\GridView;
use yii\bootstrap\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="order-index box box-primary">
    <?php Pjax::begin(); ?>
    <div class="box-header with-border">
        <?= Html::a('Get Random Prize', ['create'], ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'layout' => "{items}\n{summary}\n{pager}",
                'columns' => [
                    [
                        'attribute' => 'prizeId',
                        'value' => 'prizeTitle',
                    ],
                    [
                        'attribute' => 'status',
                        'value' => 'statusName',
                    ],
                    'createdAt:date',

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{send} {reject} {convert}',
                        'buttons' => [
                            'send' => function ($url) {
                                return Html::a('<span class="glyphicon glyphicon-send"></span>', $url, [
                                    'title' => 'Send',
                                    'aria-label' => 'Send',
                                    'data-pjax' => '0',
                                ]);
                            },
                            'reject' => function ($url) {
                                return Html::a('<span class="glyphicon glyphicon-remove"></span>', $url, [
                                    'title' => 'Reject',
                                    'aria-label' => 'Reject',
                                    'data-pjax' => '0',
                                ]);
                            },
                            'convert' => function ($url) {
                                return Html::a('<span class="glyphicon glyphicon-refresh"></span>', $url, [
                                    'title' => 'Convert',
                                    'aria-label' => 'Convert',
                                    'data-pjax' => '0',
                                ]);
                            },
                        ],
                        'visibleButtons' => [
                            'send' => function ($model) {
                                return $model->status == Order::STATUS_ACCEPTED && $model->prize->type == Prize::TYPE_POINTS;
                            },
                            'reject' => function ($model) {
                                return $model->status == Order::STATUS_ACCEPTED;
                            },
                            'convert' => function ($model) {
                                return $model->status == Order::STATUS_ACCEPTED && $model->prize->type == Prize::TYPE_MONEY;
                            },
                        ],
                    ],
                ],
            ]); ?>
        </div>
    </div>
    <?php Pjax::end(); ?>
</div>
