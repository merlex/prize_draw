<?php

namespace app\modules\front\components;

use yii\bootstrap\BootstrapAsset;
use yii\bootstrap\BootstrapPluginAsset;
use yii\web\AssetBundle;
use yii\web\YiiAsset;

class MainAsset extends AssetBundle
{

    public $sourcePath = '@app/modules/front/web';

    public $css = [
        'css/main.css'
    ];

    public $js = [
        'js/main.js',
    ];

    public $depends = [
        YiiAsset::class,
        BootstrapAsset::class,
        BootstrapPluginAsset::class,
    ];
}
