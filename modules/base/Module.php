<?php

namespace app\modules\base;

use yii\console\Application as ConsoleApplication;

/**
 * module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        // custom initialization code goes here

        $app = \Yii::$app;

        if ($app instanceof ConsoleApplication) {
            $this->controllerNamespace = 'app\modules\\' . $this->id . '\commands';
        }
    }
}
