<?php

namespace app\modules\base\services;

use app\modules\base\models\Order;
use app\modules\base\models\Prize;
use Exception;
use yii\helpers\ArrayHelper;

class BankService extends Service
{
    /**
     * @param Order $order
     * @return bool
     * @throws Exception
     */
    public function send(Order $order): bool
    {
        if (ArrayHelper::getValue($order, 'prize.type') !== Prize::TYPE_MONEY) {
            throw new Exception('Только денежный приз может быть перечислен на счет пользователя в банке!');
        }

        if (!$order->value || ($order->value < 0)) {
            throw new Exception('Сумма для отправки некорректна!');
        }

        //TODO Отправим деньги в банк

        return true;
    }
}
