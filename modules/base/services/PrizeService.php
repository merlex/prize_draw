<?php

namespace app\modules\base\services;

use app\modules\base\models\Identity;
use app\modules\base\models\Order;
use app\modules\base\models\Prize;
use Exception;
use Yii;

class PrizeService extends Service
{
    /**
     * @return Prize|array|false|null
     * @throws \yii\db\Exception
     */
    public function getRandomPrize()
    {
        $sql = 'SELECT p.id FROM {{%prize}} p'
            . ' JOIN ( SELECT RAND() * (SELECT MAX(id) FROM {{%prize}} WHERE status = "' . Prize::STATUS_ACTIVE . '") AS max_id ) AS m'
            . ' WHERE p . id >= m . max_id and p . status = "' . Prize::STATUS_ACTIVE . '"'
            . ' ORDER BY p . id ASC LIMIT 1;';

        $prizeId = Yii::$app->db->createCommand($sql)->queryScalar();
        if ($prizeId) {
            if ($prize = Prize::find()->andWhere(['id' => $prizeId])->active()->one()) {
                return $prize;
            }
        }

        return false;
    }
}
