<?php

namespace app\modules\base\services;

use app\modules\base\models\Identity;
use app\modules\base\models\Order;
use app\modules\base\models\Prize;
use app\modules\base\services\item\GiftItem;
use app\modules\base\services\item\ItemInterface;
use app\modules\base\services\item\MoneyItem;
use app\modules\base\services\item\PointsItem;
use Exception;

class OrderService extends Service
{
    /**
     * @param Prize $prize
     * @param Identity $user
     * @return Order
     */
    public function prepare(Prize $prize, Identity $user): Order
    {
        $order = new Order();
        $order->setAttributes([
            'userId' => $user->id,
            'prizeId' => $prize->id,
            'status' => Order::STATUS_ACCEPTED,
        ]);
        return $order;
    }

    /**
     * Назначает переданному пользователю приз
     *
     * @param Identity $user
     * @return ItemInterface
     * @throws Exception
     */
    public function getRandom(Identity $user)
    {
        if (!$prize = PrizeService::instance()->getRandomPrize()) {
            throw new Exception('Не удалось получить случайный приз!');
        }

        $order = $this->prepare($prize, $user);
        return $this->getOrderItem($order)->processing();
    }

    /**
     * @param $order
     * @return ItemInterface
     * @throws Exception
     */
    public function getOrderItem($order)
    {
        switch ($order->prize->type) {
            case Prize::TYPE_MONEY;
                $orderItem = new MoneyItem($order);
                break;
            case Prize::TYPE_POINTS;
                $orderItem = new PointsItem($order);
                break;
            case Prize::TYPE_GIFT;
                $orderItem = new GiftItem($order);
                break;
            default:
                throw new Exception('Неизвестный тип приза');
                break;
        }

        return $orderItem;
    }

    /**
     * @param Order $order
     * @return ItemInterface
     * @throws Exception
     */
    public function send(Order $order)
    {
        return $this->getOrderItem($order)->send();
    }

    /**
     * @param Order $order
     * @return ItemInterface
     * @throws Exception
     */
    public function reject(Order $order)
    {
        return $this->getOrderItem($order)->reject();
    }

    /**
     * @param Order $order
     * @return ItemInterface
     * @throws Exception
     */
    public function convert(Order $order)
    {
        if ($order->prize->type != Prize::TYPE_MONEY) {
            throw new Exception('Ошибка! Не подходящий тип заказа');
        }
        return $this->getOrderItem($order)->convert();
    }
}
