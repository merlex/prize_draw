<?php

namespace app\modules\base\services\item;

use app\modules\base\models\Order;
use Exception;
use Yii;

class PointsItem extends BaseItem implements ItemInterface
{
    /**
     * @return ItemInterface
     * @throws Exception
     */
    public function processing()
    {
        $settings = Yii::$app->settings;
        $maxPoints = $settings->get('SettingsForm', 'maxPoints');
        if (is_null($maxPoints) || !$maxPoints) {
            throw new Exception('Не задана максимальная сумма баллов!');
        }
        $this->order->value = rand(0, $maxPoints);

        $this->save();
        return $this;
    }

    /**
     * @return ItemInterface
     * @throws Exception
     */
    public function send()
    {
        $this->order->status = Order::STATUS_SENT;
        $this->order->user->updateCounters(['points' => $this->order->value]);
        $this->save();
        return $this;
    }

    /**
     * @return ItemInterface
     * @throws Exception
     */
    public function reject()
    {
        $this->order->status = Order::STATUS_REJECTED;
        $this->save();
        return $this;
    }
}
