<?php

namespace app\modules\base\services\item;

use app\modules\base\models\Order;
use app\modules\base\models\Prize;
use Yii;
use yii\db\Exception;

class BaseItem
{
    /** @var Order */
    protected $order;

    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->order->prize->title;
    }

    /**
     * @return mixed
     * @throws Exception
     */
    public function save()
    {
        $transaction = Yii::$app->getDb()->beginTransaction();
        if ($this->order->prize->save()) {
            if ($this->order->save()) {
                $transaction->commit();
                return $this;
            } else {
                Yii::$app->getLog()->getLogger()->logInvalidModel($this->order);
            }
        } else {
            Yii::$app->getLog()->getLogger()->logInvalidModel($this->order->prize);
        }
        $transaction->rollBack();
        throw new \Exception('Ошибка! Не удалось сохранить данные');
    }
}
