<?php

namespace app\modules\base\services\item;

interface ItemInterface
{
    /** получить название приза в заказе */
    public function getTitle();

    /** подготовка заказа */
    public function processing();

    /** отправка заказа */
    public function send();

    /** отмена заказа */
    public function reject();
}
