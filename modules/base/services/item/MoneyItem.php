<?php

namespace app\modules\base\services\item;

use Yii;
use yii\db\Exception;
use app\modules\base\models\Order;
use app\modules\base\models\Prize;
use app\modules\base\services\BankService;

class MoneyItem extends BaseItem implements ItemInterface
{
    /**
     * @return ItemInterface
     * @throws Exception
     */
    public function processing()
    {
        $settings = Yii::$app->settings;
        $maxMoney = $settings->get('SettingsForm', 'maxMoney');
        if (is_null($maxMoney) || !$maxMoney) {
            throw new Exception('Не задана максимальная сумма денежного приза!');
        }

        $maxAvailable = min($this->order->prize->available, $maxMoney);
        $this->order->value = rand(0, $maxAvailable);
        $this->order->prize->available -= $this->order->value;
        if (!$this->order->prize->available) {
            $this->order->prize->status = Prize::STATUS_INACTIVE;
        }

        $this->save();
        return $this;
    }

    /**
     * Отправляем деньги пользователю
     * @return ItemInterface
     * @throws Exception
     */
    public function send()
    {
        if (BankService::instance()->send($this->order)) {
            $this->order->status = Order::STATUS_SENT;
            $this->save();
            return $this;
        }
    }

    /**
     * Отменяем приз
     * @return ItemInterface
     * @throws Exception
     */
    public function reject()
    {
        $this->order->status = Order::STATUS_REJECTED;
        // вернем деньги и активируем приз на случай если это были последние деньги
        $this->order->prize->available += $this->order->value;
        $this->order->prize->status = Prize::STATUS_ACTIVE;
        $this->save();
        return $this;
    }

    /**
     * Конвертируем деньги в баллы
     * @return ItemInterface
     * @throws Exception
     */
    public function convert()
    {
        // получаем бонусный приз
        if (!$bonusPrize = Prize::find()->active()->type(Prize::TYPE_POINTS)->one()) {
            throw new Exception('Не задан приз "баллы лояльности"!');
        }
        $settings = Yii::$app->settings;
        $coefficient = $settings->get('SettingsForm', 'coefficient');
        // нужно проверить задан ли коэффициент для конвертации
        if (is_null($coefficient) || !$coefficient) {
            throw new Exception('Не задан коэффициент конвертации в баллы лояльности!');
        }
        // вернем деньги и активируем приз на случай если это были последние деньги
        $this->order->prize->available += $this->order->value;
        $this->order->prize->status = Prize::STATUS_ACTIVE;

        $this->order->value = round($this->order->value * $coefficient);
        $this->order->prizeId = $bonusPrize->id;
        $this->order->status = Order::STATUS_CONVERTED;
        $this->save();
        return $this;
    }
}
