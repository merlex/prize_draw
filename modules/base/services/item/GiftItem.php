<?php

namespace app\modules\base\services\item;

use app\modules\base\models\Order;
use app\modules\base\models\Prize;
use yii\db\Exception;

class GiftItem extends BaseItem implements ItemInterface
{
    /**
     * @return ItemInterface
     * @throws Exception
     */
    public function processing()
    {
        $this->order->value = 1;
        $this->order->prize->available -= 1;
        if (!$this->order->prize->available) {
            $this->order->prize->status = Prize::STATUS_INACTIVE;
        }

        $this->save();
        return $this;
    }

    /**
     * @return ItemInterface
     * @throws Exception
     */
    public function send()
    {
        $this->order->status = Order::STATUS_SENT;
        $this->save();
        return $this;
    }

    /**
     * @return ItemInterface
     * @throws Exception
     */
    public function reject()
    {
        $this->order->status = Order::STATUS_REJECTED;
        $this->order->prize->available += 1;
        $this->order->prize->status = Prize::STATUS_ACTIVE;
        $this->save();
        return $this;
    }
}
