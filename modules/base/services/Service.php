<?php

namespace app\modules\base\services;

use app\modules\base\components\Logger;
use yii\base\Component;
use yii\base\StaticInstanceTrait;

class Service extends Component
{
    use StaticInstanceTrait;

    /**
     * @param $data
     * @param string $category
     * @param int $level
     */
    public function writeLog($data, $category = 'application', $level = Logger::LEVEL_INFO)
    {
        \Yii::$app->getLog()->getLogger()->log($data, $level, $category);
    }
}
