<?php

namespace app\modules\base\components;

use yii\db\ActiveRecord;
use yii\helpers\VarDumper;

class Logger extends \yii\log\Logger
{
    /**
     * @param ActiveRecord $model
     * @param string $category
     */
    public function logInvalidModel(ActiveRecord $model, $category = 'application')
    {
        if ($model->hasErrors()) {
            $this->log(strtr("Could not :action :model\n:data\n:errors", [
                ':action' => $model->getIsNewRecord() ? 'insert' : 'update',
                ':model' => $model::className() . '[' . $model->getScenario() . ']',
                ':data' => VarDumper::dumpAsString($model->getAttributes()),
                ':errors' => VarDumper::dumpAsString($model->getErrors()),
            ]), Logger::LEVEL_ERROR, $category);
        }
    }
}
