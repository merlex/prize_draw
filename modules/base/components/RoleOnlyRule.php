<?php

namespace app\modules\base\components;

use app\modules\base\models\Identity;
use yii\rbac\Rule;

class RoleOnlyRule extends Rule
{
    /**
     * @inheritdoc
     */
    public function execute($user, $item, $params)
    {
        if (\Yii::$app->getUser()->isGuest) {
            return false;
        }

        if ($user != \Yii::$app->getUser()->getId()) {
            $identity = Identity::findOne($user);

            if (!$identity) {
                return false;
            }
        } else {
            $identity = \Yii::$app->getUser()->getIdentity();
        }

        list($roleName) = explode('-', $item->name);

        return $identity->role === $roleName;
    }
}