<?php

namespace app\modules\base\components;

use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;

class WebController extends Controller
{
    /**
     * @inheritdoc
     */
    final public function behaviors()
    {
        return [
            'accessControl' => [
                'class' => AccessControl::class,
                'rules' => $this->rules(),
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => $this->verbs(),
            ],
        ];
    }

    /**
     * @return array
     */
    protected function rules()
    {
        return [
            [
                'allow' => true,
                'roles' => ['@'],
            ],
        ];
    }

    /**
     * @return array
     */
    protected function verbs()
    {
        return [];
    }
}