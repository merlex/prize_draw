<?php

namespace app\modules\base\models;

use app\modules\base\models\queries\IdentityQuery;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "{{%identity}}".
 *
 * @property int $id
 * @property string $username
 * @property string $password
 * @property string $firstname
 * @property string $lastname
 * @property string $authKey
 * @property string $token
 * @property string $role
 * @property int $isActive
 * @property string $createdAt
 * @property string $updatedAt
 * @property int $points
 *
 * @property bool $isAdmin
 */
class Identity extends ActiveRecord implements IdentityInterface
{
    const SCENARIO_CREATE = 'create';

    const ROLE_ADMIN = 'admin';
    const ROLE_USER = 'user';

    /**
     * @var string
     */
    public $newPassword;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%identity}}';
    }

    /**
     * @inheritdoc
     * @return IdentityQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new IdentityQuery(get_called_class());
    }

    /**
     * Finds an identity by the given ID.
     * @param string|int $id the ID to be looked for
     * @return self the identity object that matches the given ID.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     */
    public static function findIdentity($id)
    {
        return static::find()
            ->where(['id' => $id])
            ->active()
            ->one();
    }

    /**
     * Finds an identity by the given token.
     * @param mixed $token the token to be looked for
     * @param mixed $type the type of the token. The value of this parameter depends on the implementation.
     * For example, [[\yii\filters\auth\HttpBearerAuth]] will set this parameter to be `yii\filters\auth\HttpBearerAuth`.
     * @return self the identity object that matches the given token.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        $identity = self::find()
            ->token($token)
            ->one();

        if ($identity && $identity->isActive) {
            return $identity;
        }

        return null;
    }

    /**
     * @param string $username
     * @param string $password
     * @return self
     */
    public static function findIdentityByUsernameAndPassword($username, $password)
    {
        $identity = static::find()
            ->username($username)
            ->active()
            ->one();

        if ($identity && $identity->validatePassword($password)) {
            return $identity;
        }

        return null;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'createdAt',
                'updatedAtAttribute' => 'updatedAt',
                'value' => function () {
                    return gmdate('Y-m-d H:i:s');
                },
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'role'], 'required'],
            [['isActive', 'points'], 'integer'],
            [['createdAt', 'updatedAt'], 'date', 'format' => 'php:Y-m-d H:i:s'],
            [['username', 'role'], 'string', 'max' => 50],
            [['password'], 'string', 'max' => 64],
            [['authKey'], 'string', 'max' => 32],
            [['username'], 'unique'],
            [['newPassword'], 'required', 'on' => static::SCENARIO_CREATE],
            [['newPassword'], 'string'],
            [['firstname', 'lastname'], 'string', 'max' => 150],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Имя пользователя',
            'password' => 'Пароль',
            'firstname' => 'Имя',
            'lastname' => 'Фамилия',
            'newPassword' => 'Новый пароль',
            'authKey' => 'Ключ авторизации',
            'role' => 'Роль',
            'roleName' => 'Роль',
            'isActive' => 'Активность',
            'createdAt' => 'Дата создания',
            'updatedAt' => 'Дата изменения',
            'points' => 'Баллы пользователя',
        ];
    }

    /**
     * Returns an ID that can uniquely identify a user identity.
     * @return string|int an ID that uniquely identifies a user identity.
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->username;
    }

    /**
     * Has Admin model instance
     * @return bool
     */
    public function getIsAdmin()
    {
        return in_array($this->role, [
            static::ROLE_ADMIN,
        ]);
    }

    /**
     * Returns a key that can be used to check the validity of a given identity ID.
     *
     * The key should be unique for each individual user, and should be persistent
     * so that it can be used to check the validity of the user identity.
     *
     * The space of such keys should be big enough to defeat potential identity attacks.
     *
     * This is required if [[User::enableAutoLogin]] is enabled.
     * @return string a key that is used to check the validity of a given identity ID.
     * @see validateAuthKey()
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * Validates the given auth key.
     *
     * This is required if [[User::enableAutoLogin]] is enabled.
     * @param string $authKey the given auth key
     * @return bool whether the given auth key is valid.
     * @see getAuthKey()
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey && $this->authKey === $authKey;
    }

    /**
     * @param string $password
     * @return bool
     */
    public function validatePassword($password)
    {
        return $this->password && \Yii::$app->getSecurity()->validatePassword($password, $this->password);
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->newPassword) {
                $this->password = \Yii::$app->getSecurity()->generatePasswordHash($this->newPassword);
                $this->authKey = \Yii::$app->getSecurity()->generateRandomString(32);
                $this->token = \Yii::$app->getSecurity()->generateRandomString(32);
                $this->newPassword = null;
            }

            return true;
        }

        return false;
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($insert) {
            $authManager = \Yii::$app->getAuthManager();

            $authManager->assign($authManager->getRole($this->role) ?: $authManager->createRole($this->role), $this->id);
        } elseif (isset($changedAttributes['role'])) {
            $authManager = \Yii::$app->getAuthManager();

            if ($role = $authManager->getRole($changedAttributes['role'])) {
                $authManager->revoke($role, $this->id);
            }

            $authManager->assign($authManager->getRole($this->role) ?: $authManager->createRole($this->role), $this->id);
        }
    }

    /**
     * @inheritdoc
     */
    public function afterDelete()
    {
        parent::afterDelete();

        \Yii::$app->getAuthManager()->revokeAll($this->id);
    }

    public function getRoleName()
    {
        $roles = self::getRoles();
        return isset($roles[$this->role]) ? $roles[$this->role] : $this->role;
    }

    /**
     * @return array
     */
    public static function getRoles()
    {
        return [
            self::ROLE_ADMIN => 'Админ',
            self::ROLE_USER => 'Пользователь',
        ];
    }


    /**
     * @return array
     */
    public function fields()
    {
        return [
            'id',
            'username',
            'authKey',
            'role',
            'isActive',
            'createdAt',
            'updatedAt',
            'firstname',
            'lastname',
            'points',
        ];
    }

    /**
     * @return string
     */
    public function getFullname()
    {
        return $this->firstname . ' ' . $this->lastname;
    }
}
