<?php

namespace app\modules\base\models;

use app\modules\base\models\queries\PrizeQuery;
use Exception;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "prize".
 *
 * @property int $id
 * @property string $title Название
 * @property string $description Описание
 * @property string $type
 * @property int $available Доступное кол-во
 * @property string $status
 * @property string|null $createdAt
 * @property string|null $updatedAt
 */
class Prize extends ActiveRecord
{
    const STATUS_ACTIVE = 'active';
    const STATUS_INACTIVE = 'inactive';

    const TYPE_MONEY = 'money';
    const TYPE_GIFT = 'gift';
    const TYPE_POINTS = 'points';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'prize';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'description'], 'required'],
            [['description', 'type', 'status'], 'string'],
            [['available'], 'integer'],
            [['createdAt', 'updatedAt'], 'safe'],
            [['title'], 'string', 'max' => 150],
            [['status'], 'in', 'range' => array_keys(static::statusNames())],
            [['type'], 'in', 'range' => array_keys(static::typeNames())],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'description' => 'Описание',
            'type' => 'Тип',
            'typeName' => 'Тип',
            'available' => 'Доступное кол-во',
            'status' => 'Статус',
            'statusName' => 'Статус',
            'createdAt' => 'Дата создания',
            'updatedAt' => 'Дата изменения',
        ];
    }

    /**
     * {@inheritdoc}
     * @return PrizeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PrizeQuery(get_called_class());
    }

    /**
     * @return array
     */
    public static function statusNames()
    {
        return [
            self::STATUS_ACTIVE => 'Активно',
            self::STATUS_INACTIVE => 'Не активно',
        ];
    }

    /**
     * @return mixed
     * @throws Exception
     */
    public function getStatusName()
    {
        return ArrayHelper::getValue(self::statusNames(), $this->status, $this->status);
    }

    /**
     * @return array
     */
    public static function typeNames()
    {
        return [
            self::TYPE_GIFT => 'Физический приз',
            self::TYPE_POINTS => 'Бонусные баллы',
            self::TYPE_MONEY => 'Деньги',
        ];
    }

    /**
     * @return mixed
     * @throws Exception
     */
    public function getTypeName()
    {
        return ArrayHelper::getValue(self::typeNames(), $this->type, $this->type);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'createdAt',
                'updatedAtAttribute' => 'updatedAt',
                'value' => function () {
                    return gmdate('Y-m-d H:i:s');
                },
            ],
        ];
    }
}
