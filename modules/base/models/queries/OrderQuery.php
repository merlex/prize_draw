<?php

namespace app\modules\base\models\queries;

use app\modules\base\models\Order;
use app\modules\base\models\Prize;

/**
 * This is the ActiveQuery class for [[\app\modules\base\models\Order]].
 *
 * @see \app\modules\base\models\Order
 */
class OrderQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Order[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Order|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @param string $value
     * @param string $condition
     * @return $this
     */
    public function status($value, $condition = '=')
    {
        return $this->andWhere([$condition, Order::tableName() . '.status', $value]);
    }

    /**
     * @param string $value
     * @param string $condition
     * @return $this
     */
    public function type($value, $condition = '=')
    {
        return $this->innerJoin(Prize::tableName(), Prize::tableName() . '.id = ' . Order::tableName() . '.prizeId')
            ->andWhere([$condition, Prize::tableName() . '.type', $value]);
    }
}
