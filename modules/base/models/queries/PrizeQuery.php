<?php

namespace app\modules\base\models\queries;

use app\modules\base\models\Prize;
use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[\app\modules\base\models\Prize]].
 *
 * @see \app\modules\base\models\Prize
 */
class PrizeQuery extends ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Prize[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Prize|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @return PrizeQuery
     */
    public function active()
    {
        return $this->andWhere([Prize::tableName() . '.status' => Prize::STATUS_ACTIVE]);
    }

    /**
     * @param string $value
     * @param string $condition
     * @return $this
     */
    public function type($value, $condition = '=')
    {
        return $this->andWhere([$condition, Prize::tableName() . '.type', $value]);
    }
}
