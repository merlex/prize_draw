<?php

namespace app\modules\base\models\queries;

use app\modules\base\models\Identity;
use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[Identity]].
 *
 * @see Identity
 */
class IdentityQuery extends ActiveQuery
{

    /**
     * @param string $value
     * @param string $condition
     * @return $this
     */
    public function token($value, $condition = '=')
    {
        return $this->andWhere([$condition, Identity::tableName() . '.token', $value]);
    }

    /**
     * @param string $value
     * @param string $condition
     * @return $this
     */
    public function username($value, $condition = '=')
    {
        return $this->andWhere([$condition, Identity::tableName() . '.username', $value]);
    }

    /**
     * @param string $value
     * @param string $condition
     * @return $this
     */
    public function role($value, $condition = '=')
    {
        return $this->andWhere([$condition, Identity::tableName() . '.role', $value]);
    }

    /**
     * @param bool $value
     * @return $this
     */
    public function active($value = true)
    {
        return $this->andWhere([Identity::tableName() . '.isActive' => $value]);
    }

    /**
     * @inheritdoc
     * @return Identity[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Identity|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
