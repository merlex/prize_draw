<?php

namespace app\modules\base\models;

use app\modules\base\models\queries\IdentityQuery;
use app\modules\base\models\queries\OrderQuery;
use app\modules\base\models\queries\PrizeQuery;
use Exception;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "order".
 *
 * @property int $id
 * @property int $userId Пользователь
 * @property int $prizeId Приз
 * @property int $value Значение
 * @property string $status
 * @property string|null $createdAt
 * @property string|null $updatedAt
 *
 * @property Prize $prize
 * @property Identity $user
 */
class Order extends ActiveRecord
{
    const STATUS_ACCEPTED = 'accepted';
    const STATUS_CONVERTED = 'converted';
    const STATUS_SENT = 'sent';
    const STATUS_REJECTED = 'rejected';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['userId', 'prizeId'], 'required'],
            [['userId', 'prizeId', 'value'], 'integer'],
            [['status'], 'string'],
            [['createdAt', 'updatedAt'], 'safe'],
            [['prizeId'], 'exist', 'skipOnError' => true, 'targetClass' => Prize::class, 'targetAttribute' => ['prizeId' => 'id']],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => Identity::class, 'targetAttribute' => ['userId' => 'id']],
            [['status'], 'in', 'range' => array_keys(static::statusNames())],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'userId' => 'Пользователь',
            'user.fullname' => 'Пользователь',
            'prizeId' => 'Приз',
            'prize.title' => 'Приз',
            'value' => 'Значение',
            'status' => 'Статус',
            'statusName' => 'Статус',
            'createdAt' => 'Дата создания',
            'updatedAt' => 'Дата изменения',
        ];
    }

    /**
     * Gets query for [[Prize]].
     *
     * @return ActiveQuery|PrizeQuery
     */
    public function getPrize()
    {
        return $this->hasOne(Prize::class, ['id' => 'prizeId']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return ActiveQuery|IdentityQuery
     */
    public function getUser()
    {
        return $this->hasOne(Identity::class, ['id' => 'userId']);
    }

    /**
     * {@inheritdoc}
     * @return OrderQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new OrderQuery(get_called_class());
    }

    /**
     * @return array
     */
    public static function statusNames()
    {
        return [
            self::STATUS_ACCEPTED => 'Зарезервирован',
            self::STATUS_CONVERTED => 'Конвертирован',
            self::STATUS_SENT => 'Отправлен',
            self::STATUS_REJECTED => 'Отклонен',
        ];
    }

    /**
     * @return mixed
     * @throws Exception
     */
    public function getStatusName()
    {
        return ArrayHelper::getValue(self::statusNames(), $this->status, $this->status);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'createdAt',
                'updatedAtAttribute' => 'updatedAt',
                'value' => function () {
                    return gmdate('Y-m-d H:i:s');
                },
            ],
        ];
    }

    /**
     * @return string
     */
    public function getPrizeTitle()
    {
        $title = $this->prize->title;
        if ($this->prize->type !== Prize::TYPE_GIFT) {
            $title .= ' - '.$this->value;
        }
        return $title;
    }
}
