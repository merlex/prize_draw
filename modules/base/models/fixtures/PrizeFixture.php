<?php
namespace app\modules\base\models\fixtures;

use yii\test\ActiveFixture;

class PrizeFixture extends ActiveFixture
{
    public $modelClass = 'app\modules\base\models\Prize';
    public $dataFile = __DIR__ . '/../../../../tests/_data/prize.php';
}
