<?php
namespace app\modules\base\models\fixtures;

use yii\test\ActiveFixture;

class OrderFixture extends ActiveFixture
{
    public $modelClass = 'app\modules\base\models\Order';
    public $dataFile = __DIR__ . '/../../../../tests/_data/order.php';
    public $depends = ['app\modules\base\models\fixtures\PrizeFixture'];
}
