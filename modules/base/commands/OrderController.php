<?php

namespace app\modules\base\commands;

use app\modules\base\models\Order;
use app\modules\base\models\Prize;
use app\modules\base\services\OrderService;
use Exception;
use yii\console\Controller;

/**
 * Order
 */
class OrderController extends Controller
{
    /**
     * Orders processing
     */
    public function actionIndex()
    {
        $this->actionMoneySend();
    }

    /**
     * Send money to bank.
     * @param int $orderLimit
     * @throws Exception
     */
    public function actionMoneySend($orderLimit = 100)
    {
        $this->stdout('Send orders to bank' . "\r\n");

        $query = Order::find()->status(Order::STATUS_ACCEPTED)->type(Prize::TYPE_MONEY)->limit($orderLimit);
        $count = 0;
        foreach ($query->batch(100) as $orders) {
            foreach ($orders as $order) {
                if (OrderService::instance()->send($order)) {
                    $count++;
                }
            }
        }

        $this->stdout("done ({$count}).\n");
    }
}
