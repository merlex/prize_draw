<?php

namespace app\modules\base\commands;

use app\modules\base\components\RoleOnlyRule;
use app\modules\base\models\Identity;
use yii\db\Query;
use yii\helpers\Console;
use yii\console\Controller;

/**
 * Role Based Access Control (RBAC)
 */
class RbacController extends Controller
{
    /**
     * @var array [ itemName => description ]
     */
    private $roles = [
        'guest' => 'Guest',
        'user' => 'User',
        'admin' => 'Administrator',
    ];

    /**
     * @var array [ ruleClass => ruleName ]
     */
    private $rules = [
        RoleOnlyRule::class => 'role-only',
    ];

    /**
     * @var array [ itemName => ruleName ]
     */
    private $permissions = [
        'admin-only' => 'role-only',
        'user-only' => 'role-only',
    ];

    /**
     * @var array [ parentName => [ childName, childName, ... ] ]
     */
    private $children = [
        'user' => ['guest', 'user-only'],
        'admin' => ['user', 'admin-only'],
    ];

    /**
     * Regenerates all auth items.
     */
    public function actionIndex()
    {
        $this->actionRemoveAll();
        $this->actionCreateRole();
        $this->actionCreateRule();
        $this->actionCreatePermission();
        $this->actionAddChild();
        $this->actionAssign();
    }

    /**
     * Removes all items.
     */
    public function actionRemoveAll()
    {
        $authManager = \Yii::$app->getAuthManager();

        $this->stdout(' * remove all items ... ');
        $authManager->removeAll();
        $this->stdout("ok\n");
    }

    /**
     * Creates role items.
     */
    public function actionCreateRole()
    {
        $authManager = \Yii::$app->getAuthManager();
        $authManager->removeAllRoles();

        foreach ($this->roles as $roleName => $description) {
            $this->stdout(' * add "' . $roleName . '" role ... ');
            $role = $authManager->createRole($roleName);
            $role->description = $description;
            $authManager->add($role);
            $this->stdout("ok\n");
        }
    }

    /**
     * Creates permission rules.
     */
    public function actionCreateRule()
    {
        $authManager = \Yii::$app->getAuthManager();
        $authManager->removeAllRules();

        foreach ($this->rules as $ruleClass => $ruleName) {
            $this->stdout(' * add "' . $ruleName . '" rule ... ');
            /** @var \yii\rbac\Rule $rule */
            $rule = new $ruleClass;
            $rule->name = $ruleName;
            $authManager->add($rule);
            $this->stdout("ok\n");
        }
    }

    /**
     * Creates permission items.
     */
    public function actionCreatePermission()
    {
        $authManager = \Yii::$app->getAuthManager();
        $authManager->removeAllPermissions();

        foreach ($this->permissions as $permissionName => $ruleName) {
            $this->stdout(' * add "' . $permissionName . '" permission ... ');
            $permission = $authManager->createPermission($permissionName);
            $permission->ruleName = $ruleName;
            $authManager->add($permission);
            $this->stdout("ok\n");
        }
    }

    /**
     * Assigns parent: child pairs.
     */
    public function actionAddChild()
    {
        $authManager = \Yii::$app->getAuthManager();

        $this->stdout(' * add children ... ');
        $ok = true;

        foreach ($this->children as $parentName => $children) {
            $parent = $authManager->getRole($parentName) ?: $authManager->getPermission($parentName);

            if (!$parent) {
                $ok = false;
                $this->stderr("\n ! parent item \"{$parentName}\" not found.", Console::FG_RED);
                continue;
            }

            $authManager->removeChildren($parent);

            foreach ($children as $childName) {
                $child = $authManager->getRole($childName) ?: $authManager->getPermission($childName);

                if (!$child) {
                    $ok = false;
                    $this->stderr("\n ! child item \"{$childName}\" not found.", Console::FG_RED);
                    continue;
                }

                $authManager->addChild($parent, $child);
            }
        }

        $this->stdout($ok ? "ok\n" : "\n");
    }

    /**
     * Assigns roles to their identities.
     */
    public function actionAssign()
    {
        $authManager = \Yii::$app->getAuthManager();
        $authManager->removeAllAssignments();

        $this->stdout(' * assign roles ... ');
        $ok = true;

        $query = (new Query)
            ->select(['id', 'role'])
            ->from(Identity::tableName())
            ->orderBy(['id' => SORT_ASC]);

        foreach ($query->each(500) as $identity) {
            $role = $authManager->getRole($identity['role']);

            if (!$role) {
                $ok = false;
                $this->stderr("\n ! role \"{$identity['role']}\" not found.", Console::FG_RED);
                continue;
            }

            $authManager->assign($role, $identity['id']);
        }

        $this->stdout($ok ? "ok\n" : "\n");
    }
}
