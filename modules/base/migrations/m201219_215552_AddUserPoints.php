<?php

use yii\db\Migration;

/**
 * Class m201219_215552_AddUserPoints
 */
class m201219_215552_AddUserPoints extends Migration
{
    public $table = '{{%identity}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn($this->table, 'points', $this->integer()->unsigned()->notNull()->defaultValue(0)->comment('Баллы пользователя'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn($this->table, 'points');
    }
}
