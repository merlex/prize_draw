<?php

use yii\db\Migration;

/**
 * Class m201217_000902_Init
 */
class m201217_000902_Init extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $now = gmdate('Y-m-d H:i:s');

        $this->createTable('{{%identity}}', [
            'id' => $this->primaryKey()->unsigned(),
            'username' => $this->string(50)->notNull()->unique(),
            'password' => $this->string(64),
            'firstname' => $this->string(150)->comment('Имя'),
            'lastname' => $this->string(150)->comment('Фамилия'),
            'authKey' => $this->string(32),
            'token' => $this->string(32)->notNull()->unique(),
            'role' => $this->string(50),
            'isActive' => $this->boolean()->unsigned()->defaultValue(1),
            'createdAt' => $this->dateTime(),
            'updatedAt' => $this->dateTime(),
        ], 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB');

        $this->createIndex('identity_role', '{{%identity}}', 'role');
        $this->createIndex('identity_isActive', '{{%identity}}', 'isActive');

        $this->batchInsert('{{%identity}}', ['username', 'password', 'authKey', 'token', 'role', 'createdAt', 'updatedAt'], [
            ['admin', \Yii::$app->getSecurity()->generatePasswordHash('admin'),
                \Yii::$app->getSecurity()->generateRandomString(32),
                \Yii::$app->getSecurity()->generateRandomString(32), 'admin', $now, $now],
        ]);

        $authManager = Yii::$app->getAuthManager();

        $adminRole = $authManager->createRole('admin');

        $authManager->add($adminRole);

        $authManager->assign($adminRole, 1);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%identity}}');

        Yii::$app->getAuthManager()->removeAll();
    }
}
