<?php

use yii\db\Migration;

/**
 * Class m201218_001054_Order
 */
class m201218_001054_Order extends Migration
{
    public $table = '{{%order}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable($this->table, [
            'id' => $this->primaryKey()->unsigned(),
            'userId' => $this->integer()->notNull()->unsigned()->comment('Пользователь'),
            'prizeId' => $this->integer()->notNull()->unsigned()->comment('Приз'),
            'value' => $this->integer()->notNull()->defaultValue(0)->unsigned()->comment('Значение'),
            'status' => "ENUM('accepted','converted','sent','rejected') NOT NULL DEFAULT 'accepted'",
            'createdAt' => $this->dateTime(),
            'updatedAt' => $this->dateTime(),
        ], 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB');

        $this->createIndex('order_status', $this->table, 'status');

        $this->addForeignKey('FK_order_userId', $this->table, 'userId', '{{%identity}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_order_prizeId', $this->table, 'prizeId', '{{%prize}}', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('FK_order_userId', $this->table);
        $this->dropForeignKey('FK_order_prizeId', $this->table);

        $this->dropTable($this->table);
    }
}
