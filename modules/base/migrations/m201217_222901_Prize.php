<?php

use yii\db\Migration;

/**
 * Class m201217_222901_Prize
 */
class m201217_222901_Prize extends Migration
{
    public $table = '{{%prize}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable($this->table, [
            'id' => $this->primaryKey()->unsigned(),
            'title' => $this->string(150)->notNull()->comment('Название'),
            'description' => $this->text()->notNull()->comment('Описание'),
            'type' => "ENUM('gift','points','money') NOT NULL DEFAULT 'gift'",
            'available' => $this->integer()->notNull()->defaultValue(0)->unsigned()->comment('Доступное кол-во'),
            'status' => "ENUM('active','inactive') NOT NULL DEFAULT 'active'",
            'createdAt' => $this->dateTime(),
            'updatedAt' => $this->dateTime(),
        ], 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB');

        $this->createIndex('prize_status', $this->table, 'status');

        $now = gmdate('Y-m-d H:i:s');
        $this->batchInsert($this->table, ['title', 'description', 'type', 'available', 'status', 'createdAt', 'updatedAt'], [
            ['Автомобиль', 'Автомобиль', 'gift', 10, 'active', $now, $now],
            ['Лаптоп', 'Лаптоп', 'gift', 100, 'active', $now, $now],
            ['Смартфон', 'Смартфон', 'gift', 1000, 'active', $now, $now],
            ['Бонусные баллы', 'Бонусные баллы', 'points', 1000, 'active', $now, $now],
            ['Денежный приз', 'Денежный приз', 'money', 1000, 'active', $now, $now],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->table);
    }
}
