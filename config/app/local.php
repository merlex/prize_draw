<?php use yii\log\FileTarget;

return [
    /*
    'bootstrap' => ['debug', 'gii'],

    'modules' => [
        'debug' => [
            'class' => \yii\debug\Module::class,
            'allowedIPs' => ['127.0.0.1', '::1'],
        ],
        'gii' => [
            'class' => 'yii\gii\Module',
            'allowedIPs' => ['127.0.0.1', '::1', '192.168.0.*', '192.168.178.20'],
            'generators' => [ //here
                'crud' => [
                    'class' => 'yii\gii\generators\crud\Generator',
                    'templates' => [
                        'adminlte' => '@vendor/dmstr/yii2-adminlte-asset/gii/templates/crud/simple',
                    ]
                ]
            ],
        ],
    ],
    */
    'components' => [
        'db' => [
            'dsn' => 'mysql:host=' . getenv('DB_HOST') . ';dbname=' . getenv('DB_NAME'),
            'username' => getenv('DB_USER'),
            'password' => getenv('DB_PASS'),
        ],
        'log' => [
            'targets' => [
                'debug' => [
                    'class' => FileTarget::class,
                    'logFile' => '@runtime/logs/debug.log',
                    'enabled' => false,
                ],
            ],
        ],
        'mailer' => [
            'messageConfig' => getenv('MAIL_FROM_EMAIL') ? [
                'from' => getenv('MAIL_FROM_NAME') ? [
                    getenv('MAIL_FROM_EMAIL') => getenv('MAIL_FROM_NAME'),
                ] : getenv('MAIL_FROM_EMAIL'),
            ] : [],
            'transport' => getenv('SMTP_HOST') && getenv('SMTP_PORT') ? [
                'class' => Swift_SmtpTransport::class,
                'host' => getenv('SMTP_HOST'),
                'port' => getenv('SMTP_PORT'),
                'username' => getenv('SMTP_USERNAME'),
                'password' => getenv('SMTP_PASSWORD'),
                'encryption' => getenv('SMTP_SECURITY') ?: 'tcp',
            ] : [],
            'useFileTransport' => true,
            'enableSwiftMailerLogging' => YII_DEBUG,
        ],
        'urlManager' => [
            'hostInfo' => getenv('BASE_URL'),
        ],
    ],

];
