<?php

use app\modules\base\components\Logger;
use yii\caching\FileCache;
use yii\db\Connection;
use yii\rbac\DbManager;
use yii\swiftmailer\Mailer;
use yii\web\UrlManager;
use yii\web\UrlNormalizer;

return [
    'id' => '',
    'name' => 'Prize Draw',
    'timeZone' => 'Europe/Moscow',
    'basePath' => '@app',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
    ],
    'bootstrap' => [],
    'components' => [
        'authManager' => [
            'class' => DbManager::class,
            'itemTable' => '{{%authItem}}',
            'itemChildTable' => '{{%authItemChild}}',
            'assignmentTable' => '{{%authAssignment}}',
            'ruleTable' => '{{%authRule}}',
        ],
        'cache' => [
            'class' => FileCache::class,
            'dirMode' => 0777,
            'fileMode' => 0666,
        ],
        'db' => [
            'class' => Connection::class,
            'dsn' => 'mysql:host=' . getenv('DB_HOST') . ';dbname=' . getenv('DB_NAME'),
            'username' => getenv('DB_USER'),
            'password' => getenv('DB_PASS'),
            'charset' => 'utf8mb4',
            'enableSchemaCache' => true,
            'schemaCacheDuration' => 3600,
            'attributes' => [
                PDO::MYSQL_ATTR_INIT_COMMAND => "SET sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''));",
            ],
        ],
        'mailer' => [
            'class' => Mailer::class,
            'htmlLayout' => false,
            'textLayout' => false,
            'messageConfig' => [
                'charset' => 'UTF-8',
            ],
            'transport' => [
                'class' => Swift_NullTransport::class,
            ],
            'useFileTransport' => true,
        ],
        'urlManager' => [
            'class' => UrlManager::class,
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'normalizer' => [
                'class' => UrlNormalizer::class,
            ],
            'rules' => [
                '' => 'front/default/index',
                'login' => 'front/default/login',
                'logout' => 'front/default/logout',
            ],
        ],
        'settings' => [
            'class' => yii2mod\settings\components\Settings::class,
        ],
    ],
    'container' => [
        'definitions' => [
            'yii\log\Logger' => [
                'class' => Logger::class,
            ],
        ],
    ],
    'modules' => [
        'gridview' => [
            'class' => '\kartik\grid\Module',
            // your other grid module settings
        ],
        'admin' => [
            'class' => \app\modules\admin\Module::class,
        ],
        'base' => [
            'class' => \app\modules\base\Module::class,
        ],
        'front' => [
            'class' => \app\modules\front\Module::class,
        ],
        'settings' => [
            'class' => \yii2mod\settings\Module::class,
        ],
    ],
    'params' => [
        'noReplyEmail' => getenv('MAIL_FROM_EMAIL'),
    ],
];
