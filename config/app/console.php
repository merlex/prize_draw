<?php

use yii\console\controllers\MigrateController;
use yii\log\FileTarget;
use yii\log\Logger;

return [
    'id' => 'console-app',
    'bootstrap' => ['log'],
    'controllerMap' => [
        'migrate' => [
            'class' => MigrateController::class,
            'useTablePrefix' => true,
            'migrationNamespaces' => [
                'yii\\queue\\db\\migrations',
            ],
            'migrationPath' => [
                '@app/modules/base/migrations',
                '@yii/rbac/migrations',
                '@vendor/yii2mod/yii2-settings/migrations'
            ],
        ],
    ],
    'controllerNamespace' => 'app\\modules\\base\\commands',
    'components' => [
        'log' => [
            'targets' => [
                'console' => [
                    'class' => FileTarget::class,
                    'levels' => Logger::LEVEL_ERROR | Logger::LEVEL_WARNING,
                    'logFile' => '@runtime/logs/console.log',
                    'dirMode' => 0777,
                    'fileMode' => 0666,
                ],
            ],
        ],
    ],
];
