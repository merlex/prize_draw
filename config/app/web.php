<?php

use app\modules\base\models\Identity;
use yii\log\FileTarget;
use yii\log\Logger;
use yii\web\Session;
use yii\web\UrlNormalizer;

return [
    'id' => 'web-app',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
    ],
    'bootstrap' => ['log'],
    'language' => 'ru-RU',
    'components' => [
        'log' => [
            'targets' => [
                'web' => [
                    'class' => FileTarget::class,
                    'levels' => Logger::LEVEL_ERROR | Logger::LEVEL_WARNING,
                    'logFile' => '@runtime/logs/web.log',
                ],
            ],
        ],
        'session' => [
            'class' => Session::class,
            'name' => 'session-id',
        ],
        'urlManager' => [
            'normalizer' => [
                'class' => UrlNormalizer::class,
            ],
        ],
        'user' => [
            'class' => yii\web\User::class,
            'identityClass' => Identity::class,
            'loginUrl' => ['front/default/login'],
            'enableAutoLogin' => true,
            'enableSession' => true,
        ],
        'view' => [
            'theme' => [
                'pathMap' => [
                    '@app/views' => '@vendor/dmstr/yii2-adminlte-asset/example-views/yiisoft/yii2-app'
                ],
            ],
        ],
    ],
];
